package main.java.com.chatclient;
/**
 *<h1>Chat client class</h1>
 *@author Patrik Gregovic
 * <br />
 * <h3>How does it work?</h3>
 * <ol>
 *     <li>Creates an runs GUI-based chat client</li>
 *     <li>Prompts user for username</li>
 *     <li>Prompts user for IP address of the server they want to connect to</li>
 *     <li>Makes connection based on IP address (port not necessary as it's currently fixed to 6868/li>
 *     <li>Sends username to server</li>
 *     <li>Sends message to server</li>
 *     <li>Accepts messages from server</li>
 *     <li>Closes the connection and exits the program</li>
 * </ol>
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ChatClient{

   /**serverAddress attribute type String*/
   String serverAddress;
   /**in object type Scanner*/
   Scanner in;
   /**out object type PrintWriter*/
   PrintWriter out;
   /**frame object type JFrame*/
   JFrame frame = new JFrame("Chat App");
   /**textField object type JTextField*/
   JTextField textField = new JTextField(50);
   /**messageArea object type JTextArea*/
   JTextArea messageArea = new JTextArea(16, 50);

   /**
     *ChatClient default constructor
     */
   public ChatClient(){
      this.serverAddress = getIP();
      if ( serverAddress== null ) System.exit(0);
      //Panel and components setup
      textField.setEditable(false);
      messageArea.setEditable(false);
      frame.getContentPane().add(textField, BorderLayout.SOUTH);
      frame.getContentPane().add(new JScrollPane(messageArea), BorderLayout.CENTER);
      frame.pack();
      frame.setLocationRelativeTo(null);
   
      //listen for enter, send message and clear textfield
      textField.addActionListener(
              e -> {
                 out.println(textField.getText());
                 textField.setText("");
              });
   }

   /**
    * getName accessor method for username form
    * @return String returns inputted username from JOptionPane prompt
    */
   private String getName(){
      messageArea.append("[Server running]\n");
      return JOptionPane.showInputDialog(frame,"Chose a name: ","Username",JOptionPane.PLAIN_MESSAGE);
   }

   /**
    * getIP accessor method for IP form
    * @return String returns inputted IP address from JOptionPane prompt
    */
   private String getIP(){
      return JOptionPane.showInputDialog(frame,"Enter IP address to connect to","IP form",JOptionPane.PLAIN_MESSAGE);
   }

   /**
    * method for appending the default waiting message while waiting on a server connection
    */
   void waiting(){
      messageArea.append("[Waiting on server]\n");
   }

   /**
    * Run method
    * @throws IOException IOException thrown upon client disconnection
    */
   void run() throws IOException{
      try{
         Socket socket = new Socket(serverAddress, 6868);
         in = new Scanner(socket.getInputStream());
         out = new PrintWriter(socket.getOutputStream(), true);
      
         while (in.hasNextLine()){
            String line = in.nextLine();
            //giveName prefix for getting name
            if (line.startsWith("giveName")){
               out.println(getName());
            }
            //takeName prefix for given name
            else if (line.startsWith("takeName")){
               this.frame.setTitle("Chat App - " + line.substring(9));
               textField.setEditable(true);
            }
            //startMessage prefix for identifying messages
            else if (line.startsWith("startMessage")){
               messageArea.append(line.substring(13) + "\n");
            }
         }
      }
      catch(java.net.ConnectException ce){
         run();
         /*Runtime.getRuntime().exec("java ChatClient",null,new File("C:/Users/pg7034/Desktop/Hw07"));
         System.exit(0);*/
      }
      finally{
         messageArea.append("[Server not running]\n");
         JOptionPane.showMessageDialog(frame,"Connection to the server is lost, no more communication will be possible.","Server connection lost",JOptionPane.INFORMATION_MESSAGE);
         run();
         /*frame.setVisible(false);
         frame.dispose();*/
      }
   }
}