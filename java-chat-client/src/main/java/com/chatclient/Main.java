package main.java.com.chatclient;

import javax.swing.*;

public class Main {
    public static void main(String[] args) throws Exception{
        ChatClient client = new ChatClient();
        client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        client.frame.setVisible(true);
        client.waiting();
        client.run();
    }
}
