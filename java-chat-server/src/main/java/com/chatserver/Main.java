package main.java.com.chatserver;

import java.net.ServerSocket;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws Exception{
        System.out.println("Server is running...");//server active feedback
        try (ServerSocket ss = new ServerSocket(6868)){ //connect to server
            while (true) {
                //keep fixed threadpool and accept information from server socket
                Executors.newFixedThreadPool(100).execute(new ChatServer.ServerHandler(ss.accept()));
            }
        }
    }
}
