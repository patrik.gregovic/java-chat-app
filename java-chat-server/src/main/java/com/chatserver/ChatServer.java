package main.java.com.chatserver;
/**
  *<h1>Chat server class</h1>
  *@author Patrik Gregovic
 * <br />
 * <h3>How does it work?</h3>
 * <ol>
 *     <li>Creates an runs a local multithreaded server on port 6868</li>
 *     <li>Accepts connections (fixed thread pool of 100)</li>
 *     <li>Accpets username from each client</li>
 *     <li>Sends username to each connected client to notify them</li>
 *     <li>Accept message from client</li>
 *     <li>Sends message to all clients</li>
 *     <li>Detect and handle users leaving the chatroom</li>
 * </ol>
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;
import java.util.HashSet;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.Date;
import java.text.SimpleDateFormat;

public class ChatServer{

   //Use HashSets instead of ArrayLists since order isnt important and it ensures no duplicates
   /**names Set type HashSet*/
   private static Set<String> names = new HashSet<>();
   /**writers Set type HashSet*/
   private static Set<PrintWriter> writers = new HashSet<>();
   
   //Handler start
   static class ServerHandler implements Runnable{
   
      /**name attribute type String*/
      private String name;
      /**socket attribute type String*/
      private Socket socket;
      /**out attribute type PrintWriter for writing output*/
      private PrintWriter out;
   
      /**
        *ServerHandler parametrized constructor
        *@param socket Socket object
        */
      public ServerHandler(Socket socket){
         this.socket = socket;
      }
   
      /**
        *Run method
        */
      public void run(){
         try {
            //define Scanner in as getInputStream from socket
            /**in attribute type Scanner for reading input*/
            Scanner in = new Scanner(socket.getInputStream());
            //define Scanner out as getOutputStream from socket
            out = new PrintWriter(socket.getOutputStream(), true);
         
            //get username and add to names HashSet
            while (true){
               out.println("giveName");
               name = in.nextLine();
               if (name == null) {
                  return; //
               }
               synchronized (names){
                  if (!names.contains(name)){
                     names.add(name);
                     break;
                  }
               }
            }
            
            //send username
            out.println("takeName " + name);
            for (PrintWriter writer : writers){
               //user joined chat feedback
               writer.println("startMessage " + name + " has joined");
            }
            System.out.println(name+" has connected");//user joined server feedback
            writers.add(out);
         
            //Recieve and handle messages from ChatClient
            try{
               while (true){
                  String input = in.nextLine();
                  int i=1;
                  for (PrintWriter writer : writers){
                     Date myDate= new Date();
                     writer.println("startMessage "+" ["+(new SimpleDateFormat("HH:mm:ss").format(myDate))+"] "+ name + ": " + input);
                     i++;
                     //check if message was sent to everyone
                     if(i==writers.size()){ System.out.println(name+" has sent a message to all "+i+" GUI clients");}
                  }
                 
               }
            }
            catch(java.util.NoSuchElementException nsee){
               System.out.println(nsee.getMessage());
            }
         } 
         catch (Exception e){
            System.out.println(e.getMessage());
         } 
         //handle users leaving
         finally{
            if (out != null) writers.remove(out);
            if (name != null){
               //user left
               names.remove(name);
               for (PrintWriter writer : writers){
                  writer.println("startMessage " + name + " has left");//user left chat feedback
               }
               System.out.println(name+" has disconnected");//user left server feedback
            }
         }
         try{
            socket.close(); // close the socket connection
         } 
         catch (IOException ioe) {
            System.out.println(ioe.getMessage());
         }
      }
   }
}