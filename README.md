# Java Chat App

Desktop chat application with client and server side developed purely in java

## How to start the server

You can start the server by running the main method or building and running a jar, and keep it running in the background.  
The current default port of the server is 6868, and it uses a fixed thread pool of 100 concurrent connections.

## How to start the client

Once a server is running, start the client either by running the main method or building and running a jar.  
You will be prompted to type in an ip address.  
Keep in mind the port number is hard-coded in the code so it's not necessary)  
Afterwards you will be asked to type in a username and from there you can use the chat from the client side.

## Contributing
Pull requests are welcome. I would like to hear your feedback on what could be improved.

## License
[MIT](https://choosealicense.com/licenses/mit/)